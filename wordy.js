
Drupal.behaviors.wordy = function (context) {
  // hide the optional fields and the checkbox description
  $('#wordy-options').hide();
  $('#edit-wordy-submit-wrapper div.description').hide();

  $('#edit-wordy-submit').click(function() {
    if ($('#edit-wordy-submit').attr('checked')) {
      // display the optional fields
      $('#edit-wordy-submit-wrapper div.description').slideDown('fast', function() {
        $('#wordy-options').slideDown('fast');
      });
      $('#edit-wordy-brief').focus();
    }
    else {
      // hide the optional fields
      $('#wordy-options').slideUp('fast', function() {
        $('#edit-wordy-submit-wrapper div.description').slideUp('fast');
      });
    }
  });
};
