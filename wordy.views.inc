<?php

/**
 * @file
 *
 */

/**
 * Implementation of hook_views_data().
 */
function wordy_views_data() {
  $data = array();

  $data['wordy']['table']['group'] = t('Wordy');
  $data['wordy']['table']['join']['node'] = array(
    'left_field' => 'vid',
    'field' => 'vid',
    'type' => 'INNER',
  );

  $data['wordy']['order_id'] = array(
    'title' => t('Order ID'),
    'help' => t('The order ID of this submission.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['wordy']['document_id'] = array(
    'title' => t('Document ID'),
    'help' => t('The document ID of this submission.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['wordy']['document_status'] = array(
    'title' => t('Document status'),
    'help' => t('The status of the submitted document.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['wordy']['payment_status'] = array(
    'title' => t('Payment status'),
    'help' => t('The status of the related payment.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['wordy']['created'] = array(
    'title' => t('Created timestamp'),
    'help' => t('When the order was created.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['wordy']['edited'] = array(
    'title' => t('Edited timestamp'),
    'help' => t('When the order was edited.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );
  $data['wordy']['completed'] = array(
    'title' => t('Completed timestamp'),
    'help' => t('When the order was completed.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
  );

  return $data;
}
