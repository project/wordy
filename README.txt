Wordy
================================================================================

Summary
--------------------------------------------------------------------------------

Provides integration with the Wordy proofreading service.


Requirements
--------------------------------------------------------------------------------

This module requires either PHP 5.2.0 or later or version 1.2.0 or later of the
json PECL extension.

This module depends on the Diff module.


Installation
--------------------------------------------------------------------------------

1. Copy the content_type_overview folder to sites/all/modules or to a
   site-specific modules folder.

2. Go to Administer > Site building > Modules and enable the Content type
   overview module.


Configuration
--------------------------------------------------------------------------------

1. Go to Administer > Site configuration > Content type overview and select the
   content types you want to display on the over view page.

You can now go to Administer > Content management > Content types and click the
new Overview tab to edit all selected content type settings in one place.


Support
--------------------------------------------------------------------------------

Please post bug reports and feature requests in the issue queue:

  http://drupal.org/project/wordy


Credits
--------------------------------------------------------------------------------

Author: Morten Wulff <wulff@ratatosk.net>


